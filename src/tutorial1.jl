module Tutorial1

import Javis
import Javis: Video, Point, Background, act!, Action, Object, anim_rotate_around, O, render, background, sethue, pos, line


function ground(args...)
    background("white")
    sethue("black")
end

function circle(p=0, color="black")
    sethue(color)
    Javis.circle(p, 25, :fill)
    return p
end

function path!(points, pos, color)
    sethue(color)
    push!(points, pos) 
    Javis.circle.(points, 2, :fill)
end

function connector(p1, p2, color)
    sethue(color)
    line(p1, p2, :stroke)
end

function run()
    myvideo = Video(500, 500)

    Background(1:70, ground) 
   
    # red ball
    red_ball = Object(1:70, (args...) -> circle(O, "red"), Point(100,0))
    act!(red_ball, Action(anim_rotate_around(2π, O)))
    
    # red ball path
    path_of_red = Point[]
    Object(1:70, (args...) -> path!(path_of_red, pos(red_ball), "red"))
    
    # blue ball
    blue_ball = Object(1:70, (args...)-> circle(O, "blue"), Point(200,80))
    act!(blue_ball, Action(anim_rotate_around(2π, 0.0, red_ball)))
    
    # blue ball path
    path_of_blue = Point[]
    Object(1:70, (args...) -> path!(path_of_blue, pos(blue_ball), "blue"))
    
    # connector
    Object(1:70, (args...) -> connector(pos(red_ball), pos(blue_ball), "black"))

    render(myvideo; pathname="circle.gif", ffmpeg_loglevel="info")
end

end # module