module Tutorial2

import Javis: Background, Object, JCircle, O, Point, Video, render, background, sethue, setdash, @JShape, circle, Action, anim_rotate_around, act!, pos, line

function ground(args...)
    background("black")
    sethue("white")
end


function run()
    frames = 1000

    myvideo = Video(500, 500)
    Background(1:frames, ground)

    R_EARTH = 200
    R_VENUS = 144

    earth = Object(1:frames, JCircle(O, 10, color="blue", action=:fill), Point(R_EARTH, 0))
    venus = Object(JCircle(O, 7, color="red", action=:fill), Point(R_VENUS, 0))

    earth_orbit = Object(@JShape begin
        sethue(color)
        setdash(edge)
        circle(O, R_EARTH, action)
    end color="white" action=:stroke edge="solid")

    venus_orbit = Object(@JShape begin
        sethue(color)
        setdash(edge)
        circle(O, R_VENUS, action)
    end color="white" action=:stroke edge="solid")

    act!(earth, Action(anim_rotate_around(12.5 * 2π * (224.7/365), O)))
    act!(venus, Action(anim_rotate_around(12.5 * 2π, O)))

    connection = []
    Object(@JShape begin
        sethue(color)
        push!(connection, [p1, p2])
        map(x -> line(x[1], x[2], :stroke), connection)
    end connection=connection p1=pos(earth) p2=pos(venus) color="gray")

    render(myvideo; pathname="cosmic_dance.gif")

end
    
end